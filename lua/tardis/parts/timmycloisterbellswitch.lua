local PART={}
PART.ID = "timmycloisterbellswitch"
PART.Name = "timmycloisterbellswitch"
PART.Model = "models/Timmy2985/Tardis/Interior/cloisterbellswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2


if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_switch.wav" ))
	end
end

TARDIS:AddPart(PART,e)