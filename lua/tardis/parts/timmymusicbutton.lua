-- timmymusicbutton

local PART={}
PART.ID = "timmymusicbutton"
PART.Name = "timmymusicbutton"
PART.Model = "models/Timmy2985/Tardis/Interior/musicbutton.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	function PART:Use()
		self:EmitSound( Sound( "Timmy2985/tardis/sounds/theme.mp3" ))
	end
end

TARDIS:AddPart(PART,e)