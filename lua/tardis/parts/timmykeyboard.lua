local PART={}
PART.ID = "timmykeyboard"
PART.Name = "timmykeyboard"
PART.Model = "models/Timmy2985/Tardis/Interior/keyboard.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "Timmy2985/tardis/sounds/keyboard.mp3" ))
	end
end

TARDIS:AddPart(PART,e)