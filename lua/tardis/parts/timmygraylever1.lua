local PART={}
PART.ID = "timmygraylever1"
PART.Name = "timmygraylever1"
PART.Model = "models/Timmy2985/Tardis/Interior/graylever1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 6

if SERVER then
	function PART:Use(activator)
		self.interior:TogglePower()
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_dial.wav" ))
	end
end

TARDIS:AddPart(PART,e)