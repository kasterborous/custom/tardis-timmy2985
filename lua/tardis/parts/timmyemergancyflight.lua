local PART={}
PART.ID = "timmyemergancyflight"
PART.Name = "timmyemergancyflight"
PART.Model = "models/Timmy2985/Tardis/Interior/telepathic.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1

if SERVER then
	function PART:Use()
	self:EmitSound( Sound( "Timmy2985/tardis/sounds/emergancyflight.mp3" ))
	local exterior=self.exterior
		if ( self:GetOn() ) then
			if exterior:GetData("vortex") then
				exterior:Mat()
				util.ScreenShake(self.interior:GetPos(),5,100,20,700)
			end
		else
			exterior:Demat()
			util.ScreenShake(self.interior:GetPos(),5,100,20,700)
		end
	end
end

TARDIS:AddPart(PART,e)