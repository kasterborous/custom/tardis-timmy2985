local PART={}
PART.ID = "timmytinytinyswitch5"
PART.Name = "timmytinytinyswitch5"
PART.Model = "models/Timmy2985/Tardis/Interior/tinytinyswitch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_tinytinyswitch.wav" ))
	end
end

TARDIS:AddPart(PART,e)