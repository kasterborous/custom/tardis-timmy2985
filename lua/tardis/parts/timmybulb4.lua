local PART={}
PART.ID = "timmybulb4"
PART.Name = "timmybulb4"
PART.Model = "models/Timmy2985/Tardis/Interior/bulb4.mdl"
PART.AutoSetup = true

if CLIENT then
	function PART:Think()
		local switch = TARDIS:GetPart(self.interior,"timmytinytinyswitch15")
		if ( switch:GetOn() ) then
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1on")
		else
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1off")
		end
	end
end

TARDIS:AddPart(PART,e)