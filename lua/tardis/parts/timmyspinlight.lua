local PART={}
PART.ID = "timmyspinlight"
PART.Name = "timmyspinlight"
PART.Model = "models/Timmy2985/Tardis/Interior/spinlight.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
		mat=Material("models/Timmy2985/Tardis/Interior/spinlightoff")
			if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("float") then
				self:SetMaterial("models/Timmy2985/Tardis/Interior/spinlighton")
			else
				self:SetMaterial("models/Timmy2985/Tardis/Interior/spinlightoff")
			end
	end
end


TARDIS:AddPart(PART)