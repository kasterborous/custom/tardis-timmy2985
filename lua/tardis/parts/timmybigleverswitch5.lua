local PART={}
PART.ID = "timmybigleverswitch5"
PART.Name = "timmybigleverswitch5"
PART.Model = "models/Timmy2985/Tardis/Interior/bigleverswitch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
	function PART:Use(activator)
		self.exterior:ToggleFlight()
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
	end
end


TARDIS:AddPart(PART)