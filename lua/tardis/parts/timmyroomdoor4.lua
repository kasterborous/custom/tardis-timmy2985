local PART={}
PART.ID = "timmyroomdoor4"
PART.Name = "timmyroomdoor4"
PART.Model = "models/Timmy2985/Tardis/Interior/roomdoor4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1


if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_doorclose.wav" ))
			self:Collide( true )
		else
			self:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_dooropen.wav" ))
			self:DontCollide( true )
		end
		

	end
end


TARDIS:AddPart(PART,e)